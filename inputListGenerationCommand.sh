# Example:
# for i in 343365 343366 343367; do for j in $(echo /scratch4/ckilby/ProductionOutput/Jan17_TTHbbLeptonic-02-04-23-01/GridDownload_sys/group.phys-higgs.mc15_13TeV.$i.*/*); do echo $j >> inputs_ttH_aMcP8_FS.txt; done; done

nomDir='/scratch4/ckilby/ProductionOutput/Mar17_TTHbbLeptonic-02-04-29/GridDownload'
sysDir='/scratch4/ckilby/ProductionOutput/Mar17_TTHbbLeptonic-02-04-29/GridDownload_sys'

# data
ls $nomDir/*period*/* &> inputs_data.txt

# ttH (aMcP8)
for i in 343365 343366 343367; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_ttH_aMcP8_FS.txt; done; done

# ttH (aMcHpp)
for i in 341177 341270 341271; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_ttH_aMcHpp_FS.txt; done; done

# ttbar (PP8 lep FS)
ls $sysDir/*410501*_s*/* &> inputs_ttbar_PP8_lep_FS.txt

# ttbar (PP8 dilep FS)
ls $sysDir/*410503*_s*/* &> inputs_ttbar_PP8_dil_FS.txt

# ttbar (PP8 lep bfil FS)
ls $sysDir/*410504*_s*/* &> inputs_ttbar_PP8_lep_bfil_FS.txt

# ttbar (PP8 dilep bfil FS)
ls $sysDir/*410505*_s*/* &> inputs_ttbar_PP8_dil_bfil_FS.txt

# ttbar (PP8 lep AFII)
ls $sysDir/*410501*_a*/* &> inputs_ttbar_PP8_lep_AFII.txt

# ttbar (PP8 dilep AFII)
ls $sysDir/*410503*_a*/* &> inputs_ttbar_PP8_dil_AFII.txt

# ttbar (PP8 lep bfil AFII)
ls $sysDir/*410504*_a*/* &> inputs_ttbar_PP8_lep_bfil_AFII.txt

# ttbar (PP8 dilep bfil AFII)
ls $sysDir/*410505*_a*/* &> inputs_ttbar_PP8_dil_bfil_AFII.txt

# ttbar (PP8up lep AFII)
ls $sysDir/*410511*_a*/* &> inputs_ttbar_PP8up_lep_AFII.txt

# ttbar (PP8down lep AFII)
ls $sysDir/*410512*_a*/* &> inputs_ttbar_PP8down_lep_AFII.txt

# ttbar (aMCP8 lep AFII)
ls $sysDir/*410225*_a*/* &> inputs_ttbar_aMcP8_lep_AFII.txt

# ttbar (aMCP8 dilep AFII)
ls $sysDir/*410226*_a*/* &> inputs_ttbar_aMcP8_dil_AFII.txt

# ttbar (aMCP8 lep bfil AFII)
ls $sysDir/*410274*_a*/* &> inputs_ttbar_aMcP8_lep_bfil_AFII.txt

# ttbar (aMCP8 dilep bfil AFII)
ls $sysDir/*410275*_a*/* &> inputs_ttbar_aMcP8_dil_bfil_AFII.txt

# ttbar (PH7 lep AFII)
ls $sysDir/*410525*_a*/* &> inputs_ttbar_PH7_lep_AFII.txt

# ttbar (PH7 dilep AFII)
ls $sysDir/*410527*_a*/* &> inputs_ttbar_PH7_dil_AFII.txt

# ttbar (PH7 lep bfil AFII)
ls $sysDir/*410528*_a*/* &> inputs_ttbar_PH7_lep_bfil_AFII.txt

# ttbar (PH7 dilep bfil AFII)
ls $sysDir/*410529*_a*/* &> inputs_ttbar_PH7_dil_bfil_AFII.txt

# ttbar (Sherpa 2.2.1 FS)
for i in 410250 410251 410252; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_ttbar_Sherpa221_FS.txt; done; done

# diboson
for i in 361063 361064 361065 361066 361067 361068 361070 361071 361072 361073 361077 361091 361092 361093 361094 361095 361096 361097; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_diboson_FS.txt; done; done

# Wtchan (prompt)
for i in 410015 410016; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_Wtchan_dilepPrompt_FS.txt; done; done

# Wtchan (fake)
for i in 410013 410014; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_Wtchan_inclFakes_FS.txt; done; done

# schan
for i in 410025 410026; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_schan_FS.txt; done; done

# tchan
for i in 410011 410012; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_tchan_FS.txt; done; done

# ttW
ls $sysDir/*410155*_s*/* &> inputs_ttW_FS.txt

# ttZ
for i in 410218 410219 410220 410156 410157; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_ttZ_FS.txt; done; done

# tV
ls $sysDir/*410150*_s*/* &> inputs_tV_FS.txt

# tWW
ls $sysDir/*410081*_s*/* &> inputs_tWW_FS.txt

# tWZ
for i in 410215 364156 364157 364158 364159 364160 364161 364162 364163 364164 364165 364166 364167 364168 364169 364170 364171 364172 364173 364174 364175 364176 364177 364178 364179 364180 364181 364182 364183 364184 364185 364186 364187 364188 364189 364190 364191 364192 364193 364194 364195 364196 364197 410215; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_tWZ_FS.txt; done; done

# tttt
ls $sysDir/*410080*_s*/* &> inputs_tttt_FS.txt

# Z+jets
for i in 364100 364101 364102 364103 364104 364105 364106 364107 364108 364110 364111 364112 364113 364114 364115 364116 364117 364118 364119 364120 364121 364122 364123 364124 364125 364126 364127 364128 364129 364130 364131 364132 364133 364134 364135 364136 364137 364138 364139 364140 364141 364198 364199 364200 364201 364202 364203 364204 364205 364206 364207 364208 364209 364210 364211 364212 364213 364214 364215; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_zjets_FS.txt; done; done

# W+jets
for i in 364156 364157 364158 364159 364160 364161 364162 364163 364164 364165 364166 364167 364168 364169 364170 364171 364172 364173 364174 364175 364176 364177 364178 364179 364180 364181 364182 364183 364184 364185 364186 364187 364188 364189 364190 364191 364192 364193 364194 364195 364196 364197; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_wjets_FS.txt; done; done

unset nomDir
unset sysDir
