float HT_all
#float NBFricoNN_dil
#float dileptonClassifBDT
#float dileptonClassifBDT_noReco
float dileptonClassifBDT_fourAt85
float dileptonClassifBDT_threetag
float dileptonClassifBDT_twoAt77
#float dileptonRecoBDT_60
#float dileptonRecoBDTnoHiggs_60
#float dileptonRecoBDT_70
#float dileptonRecoBDTnoHiggs_70
#float dileptonRecoBDT_77
#float dileptonRecoBDTnoHiggs_77
#float dileptonRecoBDT_85
#float dileptonRecoBDTnoHiggs_85
#float Mll
#float pTll
float Centrality_all
#float dRbb_MaxM
int nJets
#int nBTags
int nBTags_60
int nBTags_70
int nBTags_77
int nBTags_85
int nHFJets
jet char jet_isbtagged_60
jet char jet_isbtagged_70
jet char jet_isbtagged_77
jet char jet_isbtagged_85
# int jet_tagWeightBin
# jet float jet_mv2c10
int jet_truthmatch
# electron char el_isPrompt
# muon char mu_isPrompt
int HF_Classification
int HF_SimpleClassification
#int isTrueDIL
char truth_HDecay
int truth_top_dilep_filter
int TopHeavyFlavorFilterFlag
int ee_2015
int mumu_2015
int emu_2015
int ee_2016
int mumu_2016
int emu_2016
