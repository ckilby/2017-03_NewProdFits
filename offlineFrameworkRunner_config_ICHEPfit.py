# Config file to define a list of inputs to run through ttH-offline, with associated ttH-offline options,
# to be given to OfflineFrameworkRunner
#
# OfflineFrameworkRunner expects this config to:
#   * create a dict called inputDict
#   * fill inputDict with ProcessParams objects, with the key as the inputType of the corresponding ProcessParams
#
# Every element of the dict will be used as a single ttH-offline run

# Creating inputDict
inputDict = {}

workingDir = '/nfs/scratch4/ckilby/ttH_Work/AnalysisTop-2.4.29/2016-03_NewProdFits'

defaultConfig    = workingDir+'/offlineConfig_ICHEPlike.txt'
fakesOnlyConfig  = workingDir+'/offlineConfig_ICHEPlike_fakesOnly.txt'
promptOnlyConfig = workingDir+'/offlineConfig_ICHEPlike_promptOnly.txt'

dataConfig       = workingDir+'/offlineConfig_ICHEPlike_data.txt'

nomOnlySysts = workingDir+'/offlineConfig_ICHEPlike_systsNomOnly.txt'
allSysts     = workingDir+'/offlineConfig_ICHEPlike_systsAll.txt'

pathToInputs = workingDir+'/offFrameworkInputs/'

# Filling inputDict with desired inputs
inputDict['data']                     = ProcessParams(inputType = 'data',
                                                      configFile = dataConfig,
                                                      inputList = pathToInputs+'inputs_data.txt',
                                                      systFile = nomOnlySysts,
                                                      extraOptionsDict = {})

# inputDict['ttH_aMcP8_FS']             = ProcessParams(inputType = 'ttH_aMcP8_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttH_aMcP8_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})

# inputDict['ttH_aMcHpp_FS']            = ProcessParams(inputType = 'ttH_aMcHpp_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttH_aMcHpp_FS.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# ####################################################################################################################################

# inputDict['ttbar_PP8_lep_fakes_FS']         = ProcessParams(inputType = 'ttbar_PP8_lep_fakes_FS',
#                                                       configFile = defaultConfig,
#                                                       #configFile = fakesOnlyConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PP8_lep_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})

# inputDict['ttbar_PP8_lep_bfil_fakes_FS']    = ProcessParams(inputType = 'ttbar_PP8_lep_bfil_fakes_FS',
#                                                       configFile = defaultConfig,
#                                                       #configFile = fakesOnlyConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PP8_lep_bfil_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})

# inputDict['ttbar_PP8_dil_FS']         = ProcessParams(inputType = 'ttbar_PP8_dil_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PP8_dil_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})

# inputDict['ttbar_PP8_dil_bfil_FS']    = ProcessParams(inputType = 'ttbar_PP8_dil_bfil_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PP8_dil_bfil_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})

# inputDict['ttbar_PP8_lep_AFII']       = ProcessParams(inputType = 'ttbar_PP8_lep_AFII',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PP8_lep_AFII.txt',
#                                                       #systFile = allSysts_forAFII,
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# # inputDict['ttbar_PP8_lep_bfil_AFII']  = ProcessParams(inputType = 'ttbar_PP8_lep_bfil_AFII',
# #                                                       configFile = defaultConfig,
# #                                                       inputList = pathToInputs+'inputs_ttbar_PP8_lep_bfil_AFII.txt',
# #                                                       #systFile = allSysts_forAFII,
# #                                                       systFile = nomOnlySysts,
# #                                                       extraOptionsDict = {})

# inputDict['ttbar_PP8_dil_AFII']       = ProcessParams(inputType = 'ttbar_PP8_dil_AFII',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PP8_dil_AFII.txt',
#                                                       #systFile = allSysts_forAFII,
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['ttbar_PP8_dil_bfil_AFII']  = ProcessParams(inputType = 'ttbar_PP8_dil_bfil_AFII',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PP8_dil_bfil_AFII.txt',
#                                                       #systFile = allSysts_forAFII,
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# ####################################################################################################################################

# inputDict['ttbar_PP8up_lep_AFII']     = ProcessParams(inputType = 'ttbar_PP8up_lep_AFII',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PP8up_lep_AFII.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['ttbar_PP8down_lep_AFII']   = ProcessParams(inputType = 'ttbar_PP8down_lep_AFII',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PP8down_lep_AFII.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# # inputDict['ttbar_PH7_lep_AFII']       = ProcessParams(inputType = 'ttbar_PH7_lep_AFII',
# #                                                       configFile = defaultConfig,
# #                                                       inputList = pathToInputs+'inputs_ttbar_PH7_lep_AFII.txt',
# #                                                       systFile = nomOnlySysts,
# #                                                       extraOptionsDict = {})

# # inputDict['ttbar_PH7_lep_bfil_AFII'] = ProcessParams(inputType = 'ttbar_PH7_lep_bfil_AFII',
# #                                                       configFile = defaultConfig,
# #                                                       inputList = pathToInputs+'inputs_ttbar_PH7_lep_bfil_AFII.txt',
# #                                                       systFile = nomOnlySysts,
# #                                                       extraOptionsDict = {})

# inputDict['ttbar_PH7_dil_AFII']       = ProcessParams(inputType = 'ttbar_PH7_dil_AFII',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PH7_dil_AFII.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['ttbar_PH7_dil_bfil_AFII']  = ProcessParams(inputType = 'ttbar_PH7_dil_bfil_AFII',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PH7_dil_bfil_AFII.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# # inputDict['ttbar_aMcP8_lep_AFII']     = ProcessParams(inputType = 'ttbar_aMcP8_lep_AFII',
# #                                                       configFile = defaultConfig,
# #                                                       inputList = pathToInputs+'inputs_ttbar_aMcP8_lep_AFII.txt',
# #                                                       systFile = nomOnlySysts,
# #                                                       extraOptionsDict = {})

# # inputDict['ttbar_aMcP8_lep_bfil_AFII']= ProcessParams(inputType = 'ttbar_aMcP8_lep_bfil_AFII',
# #                                                       configFile = defaultConfig,
# #                                                       inputList = pathToInputs+'inputs_ttbar_aMcP8_lep_bfil_AFII.txt',
# #                                                       systFile = nomOnlySysts,
# #                                                       extraOptionsDict = {})

# inputDict['ttbar_aMcP8_dil_AFII']     = ProcessParams(inputType = 'ttbar_aMcP8_dil_AFII',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_aMcP8_dil_AFII.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['ttbar_aMcP8_dil_bfil_AFII']= ProcessParams(inputType = 'ttbar_aMcP8_dil_bfil_AFII',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_aMcP8_dil_bfil_AFII.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['ttbar_Sherpa221_FS']       = ProcessParams(inputType = 'ttbar_Sherpa221_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_Sherpa221_FS.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})


                                      
# inputDict['diboson_FS']               = ProcessParams(inputType = 'diboson_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_diboson_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})
                                      
# inputDict['Wtprompt_FS']              = ProcessParams(inputType = 'Wtprompt_FS',
#                                                       configFile = defaultConfig,
#                                                       #configFile = promptOnlyConfig,
#                                                       inputList = pathToInputs+'inputs_Wtchan_dilepPrompt_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})
                                      
# # inputDict['Wtfakes_FS']            =   ProcessParams(inputType = 'Wtfakes_FS',
# #                                                      configFile = defaultConfig,
# #                                                      #configFile = fakesOnlyConfig,
# #                                                      inputList = pathToInputs+'inputs_Wtchan_inclFakes_FS.txt',
# #                                                      systFile = allSysts,
# #                                                      extraOptionsDict = {})
                                      
# # inputDict['schan_FS']                 = ProcessParams(inputType = 'schan_FS',
# #                                                    configFile = defaultConfig,
# #                                                       inputList = pathToInputs+'inputs_schan_FS.txt',
# #                                                       systFile = allSysts,
# #                                                       extraOptionsDict = {})
                                      
# # inputDict['tchan_FS']                 = ProcessParams(inputType = 'tchan_FS',
# #                                                       configFile = defaultConfig,
# #                                                       inputList = pathToInputs+'inputs_tchan_FS.txt',
# #                                                       systFile = allSysts,
# #                                                       extraOptionsDict = {})
                                      
# inputDict['ttZ_FS']                   = ProcessParams(inputType = 'ttZ_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttZ_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})
                                      
# inputDict['ttW_FS']                   = ProcessParams(inputType = 'ttW_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttW_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})
                                      
# inputDict['4top_FS']                  = ProcessParams(inputType = '4top_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_tttt_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})
                                      
# inputDict['tV_FS']                    = ProcessParams(inputType = 'tV_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_tV_FS.txt',
#                                                       systFile = allSysts,
#                                                      extraOptionsDict = {})
                                      
# inputDict['tWZ_FS']                   = ProcessParams(inputType = 'tWZ_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_tWZ_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})
                                      
# inputDict['tWW_FS']                   = ProcessParams(inputType = 'tWW_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_tWW_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})
                                      
# inputDict['zjets_FS']                 = ProcessParams(inputType = 'zjets_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_zjets_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})
                                      
# inputDict['wjets_FS']                 = ProcessParams(inputType = 'wjets_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_wjets_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})
